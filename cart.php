<?php
  include "script.php";
?>

<!DOCTYPE html>
  <head>
    <html>
    <title>Корзина</title>
  </head>

  <body>

    <a href="/cart_project/product_list.php">Вернуться в список товаров</a>

    <table>
      <tr>
        <th width='50' align='left'>№</th>
        <th width='200' align='left'>Наименование</th>
        <th width='100' align='left'>Цена</th>
        <th width='150' align='left'>Количество</th>
        <th width='140' align='left'>Стоимость</th>
        <th></th>
        <th></th>
      </tr>
    </table>

    <?php
      $num = 1;
      foreach ($cart->content as $key => $purchase)
      {
        $productId = array_search($purchase["productId"], array_column($productList, "id"));
        echo "<form method='post' action='cart.php'>"
          . "<table><tr>"
          . "<td width='50'>" . $num . "</td>"
          . "<td width='200'>" . $productList[$productId]["name"] . "</td>"
          . "<td width='100'>" . $productList[$productId]["price"] . "</td>"
          . "<td width='150'>" . $purchase["quantity"] . "</td>"
          . "<td width='140'>" . $purchase["quantity"] * $productList[$productId]["price"] . "</td>"
          . "<td>"
            . "<input type='hidden' name='productId' value='" . $purchase["productId"] . "' />"
            . "<input type='text' name='quantity'>"
            . "<input type='submit' name='changeQuantity' value='Изменить количество'>"
          . "</td>"
          . "<td><input type='submit' name='deleteProduct' value='Удалить'></td>"
          . "</tr></table>"
          . "</form>";
        $num ++;
      }
    ?>

  </body>
</html>
