<?php

  $productList = array(
    array(
      "id" => 2,
      "name" => "товар1",
      "price" => 100,
    ),
    array(
      "id" => 4,
      "name" => "товар2",
      "price" => 150,
    ),
    array(
      "id" => 5,
      "name" => "товар3",
      "price" => 80,
    ),
    array(
      "id" => 1,
      "name" => "товар4",
      "price" => 10.5,
    )
  );

  session_start();
  if (!$_SESSION['cart'])
  {
    $cart = new Cart();
    $_SESSION['cart'] = $cart;
  } else {
    $cart = $_SESSION['cart'];
  }

  $productId = $_POST["productId"];
  $quantity = $_POST["quantity"];

  if ($_POST["addToCart"])
  {
    if ($quantity == "")
    {
      $quantity = 1;
    }

    if ($quantity > 0)
    {
      $cart->addToCart($productId, $quantity);
    }
    else
    {
      echo nl2br("Указано неверное количество!\n");
    }
  }

  if ($_POST["changeQuantity"])
  {
    if ($quantity == "" || $quantity < 0)
    {
      echo nl2br("Указано неверное количество!\n");
    }
    else
    {
      if ($quantity > 0)
      {
        $cart->changeQuantity($productId, $quantity);
      }
      else
      {
        $cart->deleteFromCart($productId);
      }
    }
  }

  if ($_POST["deleteProduct"])
  {
    $cart->deleteFromCart($productId);
  }



  class Cart
  {
    public $content = array();

    /**
     * Adds product to cart
     *
     * @param int $productId - id of the product
     * @param int $quantity - quantity of the product
     */
    public function addToCart(int $productId, int $quantity)
    {
      if (in_array($productId, array_column($this->content, "productId")))
      {
        $key = array_search($productId, array_column($this->content, "productId"));
        $this->content[$key]["quantity"] += $quantity;
      }
      else
      {
        $product = array(
          "productId" => $productId,
          "quantity" => $quantity
        );
        array_push($this->content, $product);
      }
    }
  
    /**
     * Changes the quantity of product in cart
     *
     * @param int $productId - id of the product
     * @param int $quantity - quantity of the product
     */
    public function changeQuantity(int $productId, int $quantity)
    {
      if (in_array($productId, array_column($this->content, "productId")))
      {
        $key = array_search($productId, array_column($this->content, "productId"));
        $this->content[$key]["quantity"] = $quantity;
      }
    }

    /**
     * Deletes product from cart
     *
     * @param int $productId - id of the product
     */

    function deleteFromCart(int $productId)
    {
      if (in_array($productId, array_column($this->content, "productId")))
      {
        $key = array_search($productId, array_column($this->content, "productId"));
        unset($this->content[$key]);
        sort($this->content);
      }
    }

  }


?>
