<?php
  include "script.php";
?>

<!DOCTYPE html>
  <head>
    <html>
    <title>Список товаров</title>
  </head>

  <body>

    <a href="/cart_project/cart.php">Перейти в корзину</a>

    <table>
      <tr>
        <th width='50' align='left'>№</th>
        <th width='200' align='left'>Наименование</th>
        <th width='100' align='left'>Цена</th>
        <th align='left'>Количество</th>
        <th></th>
      </tr>
    </table>

    <?php
      $num = 1;
      foreach ($productList as $productId => $product)
      {
        echo "<form method='post' action='cart.php'>"
          . "<table><tr>"
          . "<td width='50'>" . $num . "</td>"
          . "<td width='200'>" . $product["name"] . "</td>"
          . "<td width='100'>" . $product["price"] . "</td>"
          . "<td><input type='text' name='quantity' /></td>"
          . "<td>"
            . "<input type='hidden' name='productId' value='" . $product["id"] . "' />"
            . "<input type='submit' name='addToCart' value='Добавить в корзину' />"
          . "</td>"
          . "</tr></table>"
          . "</form>";
        $num++;
      }
    ?>

  </body>
</html>

